# ImageGalleryWebsite

## What is the purpose of this project

To create a website in which I could practice using the HTMX library and learn how to use cookies to keep someone logged in.

## What is in this website

The main page contains buttons which will send you to a admin page, login page, and a sign up page as well as all the images in the database.

When you click on an image it will take you to a details page which contains the image and all the settings used to capture it along with a description and a location

The login and sign up page have obvious functions.

The admin page lets you add images (through a file chooser), set the settings you used to capture it (ISO, shutter speed, and aperture), set the location, and write a description of the image.



